package jack.demo;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Destriptions:
 * Created by weipengjie on 2016/11/28.
 */

public class HomeFragmentListAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragments = new ArrayList<>();

    public HomeFragmentListAdapter(FragmentManager fm) {
        super(fm);
        fragments.add(RoomCalendarFragment.newInstance("room_calendar_fragment"));
        fragments.add(OrderManageFragment.newInstance("order_manage_fragment"));
        fragments.add(InventoryFragment.newInstance("inventory_fragment"));
        fragments.add(PersonalFragment.newInstance("personal_fragment"));
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
