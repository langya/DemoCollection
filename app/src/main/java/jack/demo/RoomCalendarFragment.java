package jack.demo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Destriptions:
 * Created by weipengjie on 2016/11/28.
 */
public class RoomCalendarFragment extends LazyLoadFragment {
    private View rootView;
    private String name;
    @Bind(R.id.tv_fragment_room_calendar_page_name)
    TextView tvPageName;

    @Override
    protected void loadData() {

    }

    public static RoomCalendarFragment newInstance(String name) {
        RoomCalendarFragment fragment = new RoomCalendarFragment();
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (null != parent) {
                parent.removeView(rootView);
            }
        } else {
            rootView = inflater.inflate(R.layout.fragment_room_calendar, container, false);
            ButterKnife.bind(this, rootView);
        }
        Log.d("RoomCalendarFragment", "onCreateView");
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        name = bundle.getString("name");
        tvPageName.setText(name);
        super.onActivityCreated(savedInstanceState);
    }
}
